#!make

MAKEFLAGS = --no-print-directory

CI_REGISTRY_IMAGE ?= node-conf
TAG ?= dev

all: build

.PHONY: build
build:
	docker build \
		-t ${CI_REGISTRY_IMAGE}:${TAG} \
		.
	@echo "Built image:"
	@echo "  ${CI_REGISTRY_IMAGE}:${TAG}"

.PHONY: shell
shell:
	docker run --rm -it --entrypoint sh ${CI_REGISTRY_IMAGE}:${TAG}

.PHONY: build-push
build-push: build
build-push:
	docker push ${CI_REGISTRY_IMAGE}:${TAG}

FROM alpine:3.17

# Install devcontainers/cli requirements
RUN apk add --update --no-cache bash curl jq
